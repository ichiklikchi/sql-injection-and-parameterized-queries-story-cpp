# SQL Injection Story

**To read**: [https://cheatsheetseries.owasp.org/cheatsheets/SQL_Injection_Prevention_Cheat_Sheet.html]

**Estimated reading time**: 10 min

## Story Outline

This story is focused on providing guidance for preventing SQL Injection flaws
in your applications.

SQL injection is a web security vulnerability that allows an attacker 
to interfere with the queries that an application makes to its database. 
It generally allows an attacker to view data that they are not normally able to retrieve.

In some situations, an attacker can escalate an SQL injection attack 
to compromise the underlying server or other back-end infrastructure, 
or perform a denial-of-service attack.

## Story Organization

**Story Branch**: master
>`git checkout master`

**Practical task tag for self-study**: task
>`git checkout task`

Tags: #injection, #security, #sql
