CREATE TABLE IF NOT EXISTS user.credentials
(
    user_id  INTEGER PRIMARY KEY,    
    username VARCHAR(200),
    userpass VARCHAR(200)
);