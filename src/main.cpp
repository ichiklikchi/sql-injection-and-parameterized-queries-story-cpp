#include "AuthenticationApp.h"

int main() {
    sql_inj::AuthenticationApp app;
    app.run();
    
    return 0;
}
