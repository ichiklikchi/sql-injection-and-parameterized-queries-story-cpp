#pragma once
#include "IStorage.h"
#include <memory>

namespace sql {
    class Connection;
    class Statement;
}

namespace sql_inj {

    class SQLStorage : public IStorage {
        public:
            SQLStorage();

            bool hasUser(
                std::string_view login, 
                std::string_view password) const override;
        private:
            std::unique_ptr<sql::Connection> m_connection = nullptr;
    };
}
