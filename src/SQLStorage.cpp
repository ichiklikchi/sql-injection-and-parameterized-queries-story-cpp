#include "SQLStorage.h"
#include <fstream>
#include <sstream>
#include <cppconn/driver.h>
#include <cppconn/exception.h>
#include <cppconn/resultset.h>
#include <cppconn/statement.h>
#include <cppconn/prepared_statement.h>

namespace sql_inj {

    std::string toString(const std::string& path);

    SQLStorage::SQLStorage() {
        sql::Driver* driver = get_driver_instance();
        m_connection.reset(driver->connect("tcp://127.0.0.1:3306", "root", "very_strong_password"));
        m_connection->setSchema("user");
        std::unique_ptr<sql::Statement> statement(m_connection->createStatement());
        statement->execute(toString("./resources/create.sql"));
        statement->execute(toString("./resources/fill.sql"));
    }

    bool SQLStorage::hasUser(std::string_view login, std::string_view password) const {
        std::string select{"select user_id from credentials where username = ?"};
        if (not password.empty()) {
            select += " and userpass = ?";
        }
        auto preStatement = m_connection->prepareStatement(select);
        preStatement->setString(1, login.data());
        if (not password.empty()) {
            preStatement->setString(2, password.data());
        }
        sql::ResultSet* res = preStatement->executeQuery();
        return res ? res->next(): false; 
    }

    std::string toString(const std::string& path) {
        std::ifstream file(path);
        std::stringstream buffer;
        buffer << file.rdbuf();
        return buffer.str();
    }
    
    StoragePtr createStorage() {
        return std::make_unique<SQLStorage>();
    }
}
